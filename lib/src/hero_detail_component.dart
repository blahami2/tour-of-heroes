import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';
import 'package:angular2/platform/common.dart';
import 'dart:async';

import 'hero_service.dart';
import 'hero.dart';


@Component(
  selector: 'hero-detail',
  // defines identifier, which will then be used in other component's template like that: <hero-detail>
  templateUrl: 'hero_detail_component.html',
  styleUrls: const ['hero_detail_component.css'],
  directives: const [COMMON_DIRECTIVES],
)
class HeroDetailComponent implements OnInit {
  final HeroService _heroService;
  final RouteParams _routeParams;
  final Location _location;

  HeroDetailComponent(this._heroService, this._routeParams, this._location);

  // @Input() // make it able to be target of a binding expression
  Hero hero;

  @override
  ngOnInit() async {
    var _id = _routeParams.get('id');
    var id = int.parse(_id ?? '', onError: (_) => null);
    if (id != null) hero = await (_heroService.getHero(id));
  }

  void goBack() => _location.back();

  Future<Null> save() async {
    _heroService.update(hero);
    goBack();
  }
}