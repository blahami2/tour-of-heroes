import 'dart:async';
import 'dart:convert';

import 'package:angular2/angular2.dart';
import 'package:http/http.dart';

import 'hero.dart';

@Injectable() // this service can be injected
class HeroService {
  static const _heroesUrl = 'api/heroes'; // URL to web API
  static final _headers = {'Content-Type': 'application/json'};
  final Client _http;

  HeroService(this._http);

  //  Future<List<Hero>> getHeroes() async => mockHeroes;
//  Future<List<Hero>> getHeroes() {
//    return new Future.delayed(
//        const Duration(
//            milliseconds: 100), () async => mockHeroes); // return delayed asynchronous method (lambda)
//  }

//  Future<Hero> getHero(int id) async {
//    return mockHeroes
//        .where((hero) => hero.id == id)
//        .first;
//  }

  Future<List<Hero>> getHeroes() async {
    try {
      final response = await _http.get(_heroesUrl);
      final heroes = _extractData(response)
          .map((value) => new Hero.fromJson(value))
          .toList();
      return heroes;
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<Hero> getHero(int id) async {
    try {
      final response = await _http.get('$_heroesUrl/$id');
      return new Hero.fromJson(_extractData(response));
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<Hero> update(Hero hero) async {
    try {
      final response = await _http.put(
          '$_heroesUrl/${hero.id}', headers: _headers, body: JSON.encode(hero));
      return new Hero.fromJson(_extractData(response));
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<Hero> create(String name) async {
    try {
      final response = await _http.post(
          '$_heroesUrl', headers: _headers, body: JSON.encode({'name': name}));
      return new Hero.fromJson(_extractData(response));
    } catch (e) {
      throw _handleError(e);
    }
  }

  Future<Null> delete(Hero hero) async {
    try {
      final response = await _http.delete('$_heroesUrl/${hero.id}');
    } catch (e) {
      throw _handleError(e);
    }
  }

  dynamic _extractData(Response response) => JSON.decode(response.body);

  Exception _handleError(dynamic e) {
    print(e); // for demo purposes only
    return new Exception('Server error; cause: $e');
  }
}
