import 'dart:async';
import 'package:angular2/angular2.dart';
import 'package:angular2/router.dart';

import 'hero.dart';
import 'hero_service.dart';

@Component(
  selector: 'my-heroes',
  templateUrl: 'heroes_component.html',
  styleUrls: const ['heroes_component.css'],
  directives: const [COMMON_DIRECTIVES],
  pipes: const [COMMON_PIPES],
  providers: const [HeroService],
)
class HeroesComponent implements OnInit {
  final HeroService _heroService;
  final Router _router;

  List<Hero> heroes;
  Hero selectedHero;

  HeroesComponent(this._heroService, this._router);

  void onSelect(Hero hero) {
    selectedHero = hero;
  }

  Future<Null> gotoDetail() =>
      _router.navigate([
        'HeroDetail',
        {'id': selectedHero.id.toString()}
      ]);

  Future<Null> add(String name) async {
    name = name.trim();
    if (name.isEmpty) return;
    heroes.add(await _heroService.create(name));
    selectedHero = null;
  }

  Future<Null> delete(Hero hero) async {
    // stops propagation in order to prevent selecting, see html
    await _heroService.delete(hero);
    heroes.remove(hero); // cached heroes (displayed)
    if (selectedHero == hero) selectedHero = null;
  }

  @override
  ngOnInit() async {
//    _heroService.getHeroes().then((heroes) => this.heroes = heroes); // using .then method
//    heroes = await _heroService.getHeroes(); // using await
    initHeroes();
  }

  Future<Null> initHeroes() async {
    heroes = await _heroService.getHeroes();
  }
}